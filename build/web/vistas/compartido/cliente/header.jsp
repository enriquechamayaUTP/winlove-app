<%-- 
    Document   : cabecera
    Created on : 05/04/2021, 11:57:36 PM
    Author     : echamaya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../css_general.jsp" %>    
    </head>

    <body class="hold-transition skin-blue layout-top-nav">
        <div class="wrapper">
            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="../index.html" class="navbar-brand"><b>WIN</b>LOVE</a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="#">Catálogo <span class="sr-only">(current)</span></a></li>
                                <li><a href="#">Ofertas</a></li>
                                <li><a href="#">Mochilas</a></li>
                                <li><a href="#">Bolsos</a></li>
                                <li><a href="#">Accesorios</a></li>                                
                                <li><a href="#">Contáctanos</a></li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- Tasks Menu -->
                                <li class="dropdown tasks-menu">
                                    <!-- Menu Toggle Button -->
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span class="label label-danger">0</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">Tiene 4 productos en el carrito</li>
                                        <li>
                                            <ul class="menu">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-tags text-blue"></i> Producto 1
                                                        <span class="pull-right-container">
                                                            <small class="label pull-right bg-blue">S/. 99.99</small>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-tags text-blue"></i> Producto 2
                                                        <span class="pull-right-container">
                                                            <small class="label pull-right bg-blue">S/. 299.99</small>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="footer"><a href="#">Ir al carrito</a></li>
                                    </ul>
                                </li>
                                <%@include file="../componente-login.jsp" %>
                            </ul>
                        </div>
                        <!-- /.navbar-custom-menu -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </header>
            <div class="content-wrapper">
                <div class="container">