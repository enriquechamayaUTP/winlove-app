<%-- 
    Document   : footer
    Created on : 06/04/2021, 12:02:25 AM
    Author     : echamaya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
</div>
</div>
<footer class="main-footer">
    <div class="container">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2021 Grupo Winlove.</strong> Todos los derechos reservados.
    </div>
    <!-- /.container -->
</footer>
</div>
<%@include file="../js_general.jsp" %>
</body>
</html>