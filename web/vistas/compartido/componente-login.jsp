<%-- 
    Document   : componente-login
    Created on : 08/04/2021, 10:48:58 PM
    Author     : echamaya
--%>

<%@page import="winlove.app.utils.TipoUsuarioEnum"%>
<%@page import="winlove.app.dto.ObtenerUsuarioLogueado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
//    ObtenerUsuarioLogueado usuarioLogueado = (ObtenerUsuarioLogueado) session.getAttribute("usuario");
    ObtenerUsuarioLogueado usuarioLogueado = new ObtenerUsuarioLogueado();
    usuarioLogueado.setTipoUsuario(1);
%>

<%if (usuarioLogueado != null) {%>
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-user-circle"></i>
        <span class="hidden-xs">Alexander Pierce</span>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="#">Ver perfil</a></li>
        <%if (usuarioLogueado.getTipoUsuario() == TipoUsuarioEnum.CLIENTE.getValue()) {%>
        <li><a href="#">Ver mis pedidos</a></li>
        <%} else if (usuarioLogueado.getTipoUsuario() == TipoUsuarioEnum.ADMINISTRADOR.getValue()) {%>
        <li><a href="#">Configuración</a></li>
        <%}%>
        <li class="divider"></li>
        <li><a href="#">Cerrar sesión</a></li>
    </ul>
</li>
<%} else {%>
<li>
    <a href="../vistas/login.jsp" class="">
        <i class="fa fa-sign-in"></i>&nbsp;
        Ingresar
    </a>
</li>
<%}%>