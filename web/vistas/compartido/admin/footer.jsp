<%-- 
    Document   : footer
    Created on : 07/04/2021, 08:04:09 PM
    Author     : echamaya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
</section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2021 Grupo Winlove.</strong> Todos los derechos reservados.
</footer>

</div>
<!-- ./wrapper -->
<%@include file="../js_general.jsp" %>
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree();
    });
</script>
</body>
</html>