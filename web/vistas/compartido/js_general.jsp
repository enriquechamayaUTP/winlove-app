<%-- 
    Document   : js_general
    Created on : 06/04/2021, 12:07:28 AM
    Author     : echamaya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="../librerias/jquery/jquery-3.6.0.min.js" type="text/javascript"></script>
<script src="../librerias/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="../librerias/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="../plantilla/dist/js/adminlte.min.js" type="text/javascript"></script>
<script src="../librerias/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>