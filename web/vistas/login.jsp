<%-- 
    Document   : login
    Created on : 08/04/2021, 11:20:42 PM
    Author     : echamaya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="compartido/css_general.jsp" %>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="../../index2.html"><b>WIN</b>LOVE</a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Inicie sesión o regístrese</p>
                <form action="../../index2.html" method="post">
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Correo electrónico">
                        <span class="fa fa-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Contraseña">
                        <span class="fa fa-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-xs-offset-3">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar sesion</button>
                        </div>
                    </div>
                </form>
                <div class="social-auth-links text-center">
                    <p>- OTRAS OPCIONES -</p>
                    <a href="#">Recuperar contraseña</a><br>
                    <a href="register.html" class="text-center">Regístrese</a>
                </div>
            </div>
        </div>

        <%@include file="compartido/js_general.jsp" %>
    </body>
</html>
