$(function () {
    function listarPersona() {
        $.post({
            url: "../PersonaController",
            data: {accion: "listarPersona"},
            beforeSend: () => {
                var listaPersona = $("#listaPersona");
                listaPersona.append(`
                <tr>
                    <td colspan="4" style="text-align: center">
                        Cargando datos...
                    </td>
                </tr>
                `);
            },
            success: (data) => {
                var listaPersona = $("#listaPersona");
                listaPersona.html('');
                $.each(data, function (index, elemento) {
                    listaPersona.append(`
                    <tr>
                        <td>${elemento.id}</td>
                        <td>${elemento.nombre}</td>
                        <td>${elemento.apellidoPaterno}</td>
                        <td>${elemento.apellidoMaterno}</td>
                    </tr>
                    `);
                });
                console.log(data);
            },
            error: () => {
                console.log("error");
            }
        });
    }

    listarPersona();

});