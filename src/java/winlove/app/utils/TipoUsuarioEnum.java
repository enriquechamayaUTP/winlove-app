package winlove.app.utils;

/**
 * @author echamaya
 */
public enum TipoUsuarioEnum {
    ADMINISTRADOR(1),
    VENDEDOR(2),
    CLIENTE(3);

    public int id;

    private TipoUsuarioEnum(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }
}
