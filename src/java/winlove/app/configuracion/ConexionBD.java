package winlove.app.configuracion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author echamaya
 */
public class ConexionBD {

    final static Logger LOG = LoggerFactory.getLogger(ConexionBD.class);

    public static Connection conectarBD() {
        LOG.info("Inicio del proceso conectarBD");
        String usuario = "tcipos";
        String contrasena = "12345";
        String url = "jdbc:mysql://localhost:3306/winlovebd?serverTimezone=UTC";
        String driver = "com.mysql.cj.jdbc.Driver";
        Connection cn = null;
        try {
            Class.forName(driver);
            cn = DriverManager.getConnection(url, usuario, contrasena);
        } catch (SQLException | ClassNotFoundException ex) {
            LOG.error("Error conectarBD", ex);
        }
        LOG.info("Fin del proceso conectarBD");
        return cn;
    }

    public static void desconectarBD(Connection cn, ResultSet rs, PreparedStatement ps) {
        LOG.info("Inicio del proceso desconectarBD");
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }

        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }

        if (cn != null) {
            try {
                cn.close();
            } catch (SQLException e) {
            }
        }
        LOG.info("Fin del proceso desconectarBD");
    }

}
