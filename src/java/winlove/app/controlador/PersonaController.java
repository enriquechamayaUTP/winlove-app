/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package winlove.app.controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import winlove.app.modelo.Persona;
import winlove.app.servicio.PersonaService;

/**
 *
 * @author echamaya
 */
public class PersonaController extends HttpServlet {

    PersonaService personaService;

    @Override
    public void init() throws ServletException {
        personaService = new PersonaService();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Enumeration<String> parameterNames = req.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            System.out.println(paramName);
            System.out.println("n");

            String[] paramValues = req.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                System.out.println("t" + paramValue);
                System.out.println("n");
            }
        }

        String accion = req.getParameter("accion");
        System.out.println(accion);
        if (accion != null) {
            switch (accion) {
                case "listarPersona":
                    listarPersona(req, res);
                    break;
                default:
                    mostrarError(req, res);
            }
        } else {
            mostrarError(req, res);
        }
    }

    private void listarPersona(HttpServletRequest req, HttpServletResponse res) throws IOException {
        PrintWriter out = res.getWriter();
        res.setContentType("application/json");
        List<Persona> personaList = personaService.listar();
        out.print(new Gson().toJson(personaList));
    }

    private void mostrarError(HttpServletRequest req, HttpServletResponse res) throws IOException {
        PrintWriter out = res.getWriter();
        out.print("gaaaaaa");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("entro aquí");
        super.doPost(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

}
