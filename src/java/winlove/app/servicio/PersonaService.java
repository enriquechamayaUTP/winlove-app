package winlove.app.servicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import winlove.app.configuracion.ConexionBD;
import winlove.app.modelo.Persona;

/**
 * @author echamaya
 */
public class PersonaService {

    final Logger LOG = LoggerFactory.getLogger(PersonaService.class);

    public List<Persona> listar() {
        LOG.info("Inicio del proceso listar persona");
        Connection cn = ConexionBD.conectarBD();
        final String SQL = "select * from persona";
        ArrayList personaList = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = cn.prepareStatement(SQL);
            rs = ps.executeQuery();
            while (rs.next()) {
                Persona persona = new Persona();
                persona.setId(rs.getInt("id"));
                persona.setNombre(rs.getString("nombre"));
                persona.setApellidoPaterno(rs.getString("apellidoPaterno"));
                persona.setApellidoMaterno(rs.getString("apellidoMaterno"));
                personaList.add(persona);
            }
        } catch (SQLException ex) {
            LOG.error("Error listar", ex);
        }
        ConexionBD.desconectarBD(cn, rs, ps);
        LOG.info("Fin del proceso listar persona: {}", personaList.toString());
        return personaList;
    }

}
